package in.co.dhdigital.processors;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.utils.UUIDs;

import in.co.dhdigital.models.Destination;
import in.co.dhdigital.repositories.DestinationRepository;

public class WriteCassandraProcessor implements Processor {

	@Autowired
	DestinationRepository destinationRespository;
	@Override
	public void process(Exchange exchange) throws Exception {

		List<Destination> destinationList = new ArrayList<Destination>();
		destinationList = (List<Destination>) exchange.getIn().getBody();
		for(int i =0;i<destinationList.size();i++) {
			destinationList.get(i).setId( UUIDs.timeBased());
			destinationRespository.save(destinationList.get(i));
		}
	}
	
}
