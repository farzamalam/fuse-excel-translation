package in.co.dhdigital.processors;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import in.co.dhdigital.models.Destination;
import in.co.dhdigital.models.Source;

public class MapSourceToDestinationProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		List<Source> sourceList = new ArrayList<Source>();
		sourceList = (List<Source>) exchange.getIn().getBody();
		
		List<Destination> destinationList = new ArrayList<Destination>();
		
		for(int i =0;i<sourceList.size();i++) {
			Source source = sourceList.get(i);
			Destination destination = new Destination();
			destination.setSupplierGSTIN(source.getSupplierGSTIN());
			destination.setTransactionType(source.getDocumentType());
			destination.setOriginalTransactionType("");
			destination.setNatureOfDocument(source.getDocumentType());
			destination.setCustomerGSTIN(source.getCustomerGSTIN());
			destination.seteCommGSTIN(source.geteCommGSTIN());
			destination.setCustomerName(source.getCustomerName());
			destination.setErpTransactionID(source.getAccountVoucherNumber());
			destination.setDocumentNumber(source.getDocumentNumber());
			destination.setDocumentDate(source.getDocumentDate());
			destination.setLineItemID(source.getLineNumber());
			destination.setGoodsOrService("GOODS");
			destination.setHsn(source.getHsnOrSAC());
			destination.setHsnDescription(source.getProductDescription());
			destination.setQuantity(source.getQuantity());
			destination.setUqc(source.getUnitOfMeasurement());
			destination.setPlaceOfSupply(source.getPos());
			destination.setSection7Supply("N");
			destination.setNetTaxableValue(source.getTaxableValue());
			destination.setIgstRate(source.getIntegratedTaxRate());
			destination.setIgstAmount(source.getIntegratedTaxAmount());
			destination.setCgstRate(source.getCentralTaxRate());
			destination.setCgstAmount(source.getCentralTaxAmount());
			destination.setSgstRate(source.getStateUTTaxRate());
			destination.setSgstAmount(source.getStateUTTaxAmount());
			destination.setCessRate(source.getCessRateAdvalorem());
			destination.setCessAmount(source.getCessAmountAdvalorem());
			destination.setInvoiceValue(source.getInvoiceValue());
			destination.setSupplyCategory(source.getSupplyType());
			destination.setRcm(source.getReverseChargeFlag());
			destination.setDivision(source.getDivision());
			destination.setSubLocation(source.getPlantCode());
			destination.setShipToStateCode(source.getShipToState());
			destination.setFinancialPeriod(source.getReturnPeriod());
			
			destinationList.add(destination);
		}
		System.out.println("HsnOrSAC : "+ sourceList.get(0).getHsnOrSAC());
		System.out.println("destinationList.size() : "+ destinationList.size());
		exchange.getIn().setBody(destinationList);;
		
	}

}





















