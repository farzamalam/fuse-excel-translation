package in.co.dhdigital.processors;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import in.co.dhdigital.models.Destination;

public class TransformationProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		List<Destination> destinationList = new ArrayList<Destination>();
		destinationList = (List<Destination>) exchange.getIn().getBody();
		
		for(int i =0;i<destinationList.size();i++) {
			Destination destination = destinationList.get(i);
			
			if(destination.getTransactionType().equals("INV")) {
				destination.setTransactionType("Regular Invoice");
			}
			
			if(destination.getNatureOfDocument().equals("INV")) {
				destination.setNatureOfDocument("Invoices for outward supply");
			}
			
			if(destination.geteCommGSTIN().equals("0")) {
				destination.seteCommGSTIN(" ");
			}
			
			if(destination.getUqc().equals("OTH-OTHERS")) {
				destination.setUqc("OTH");
			}
			
			destination.setIgstAmount(Math.round(destination.getIgstAmount() * 100.0) / 100.0);
			destination.setCgstAmount(Math.round(destination.getCgstAmount() * 100.0) / 100.0);
			destination.setSgstAmount(Math.round(destination.getSgstAmount() * 100.0) / 100.0);
			destination.setCessAmount(Math.round(destination.getCessAmount() * 100.0) / 100.0);
			destination.setInvoiceValue(Math.round(destination.getInvoiceValue() * 100.0) / 100.0);
			
			if(destination.getSupplyCategory().equals("TAX")) {
				destination.setSupplyCategory("Taxable");
			}
			
			// Validations.
			if(destination.getSupplierGSTIN().equals("")) {
				destination.setRemarks("Supplier GSTIN is mandatory. But not available in source excel.");
			}
			if(destination.getTransactionType().equals("")) {
				destination.setRemarks("Transaction Type is mandatory. But not available in source excel.");
			}
			if(destination.getNatureOfDocument().equals("")) {
				destination.setRemarks("Nature of Document is mandatory. But not available in source excel.");
			}
			if(destination.getDocumentNumber().equals("")) {
				destination.setRemarks("Document Number is mandatory. But not available in source excel.");
			}
			if(destination.getDocumentDate() == null) {
				destination.setRemarks("Document date is mandatory. But not available in source excel.");
			}
			if(destination.getHsn().equals("")) {
				destination.setRemarks("HSN is mandatory. But not available in source excel.");
			}
			if(destination.getUqc().equals("")) {
				destination.setRemarks("UQC is mandatory. But not available in source excel.");
			}
			if(destination.getPlaceOfSupply().equals("")) {
				destination.setRemarks("Place of Supply is mandatory. But not available in source excel.");
			}
			if(destination.getSupplyCategory().equals("")) {
				destination.setRemarks("Supply Category is mandatory. But not available in source excel.");
			}
			if(destination.getRcm().equals("")) {
				destination.setRemarks("RCM is mandatory. But not available in source excel.");
			}
			
		}
		int count = 0;
		for(int i =0;i<destinationList.size();i++) {
			if(destinationList.get(i).getRemarks() != null && !destinationList.get(i).getRemarks().equals("")) {
				count ++;
			}
		}
		System.out.println("Invalid entries : "+count);
		exchange.getIn().setBody(destinationList);
	}

}
