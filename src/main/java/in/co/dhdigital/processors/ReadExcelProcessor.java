package in.co.dhdigital.processors;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import in.co.dhdigital.models.Source;

public class ReadExcelProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		System.out.println("Inside ReadExcelProcessor.process()");

		String filePath = exchange.getIn().getHeader("CamelFilePath").toString();

		// Get a file object, workbook and sheet object.
		FileInputStream file = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(file);
		Sheet sheet = workbook.getSheetAt(0);

		// Initialize source list.
		List<Source> sourceList = new ArrayList<Source>();
		int lastRow = sheet.getLastRowNum();

		System.out.println("Last Row : " + lastRow);
		for (int rn = 2; rn <= lastRow; rn++) {
			Source source = new Source();
			Row row = sheet.getRow(rn);
			
			source.setSourceIdenitifier(getStringField(row.getCell(0)));
			source.setSourceFileName(getStringField(row.getCell(1)));
			source.setGlaAccountCode(getStringField(row.getCell(2)));
			source.setDivision(getStringField(row.getCell(3)));
			source.setSubDivision(getStringField(row.getCell(4)));
			source.setProfitCenter1(getStringField(row.getCell(5)));
			source.setProfitCenter2(getStringField(row.getCell(6)));
			source.setPlantCode(getStringField(row.getCell(7)));
			source.setReturnPeriod(getStringField(row.getCell(8)));
			source.setSupplierGSTIN(getStringField(row.getCell(9)));
			source.setState(getStringField(row.getCell(10)));
			source.setDocumentType(getStringField(row.getCell(11)));
			source.setSupplyType(getStringField(row.getCell(12)));
			source.setDocumentNumber(getStringField(row.getCell(13)));
			source.setDocumentDate(getDateField(row.getCell(14)));
			source.setOriginalDocumentNumber(getStringField(row.getCell(15)));
			source.setOriginalDocumentDate(getDateField(row.getCell(16)));
			source.setCrdrPreGST(getStringField(row.getCell(17)));
			source.setLineNumber((int) (getNumericField((row.getCell(18)))));
			source.setCustomerGSTIN(getStringField(row.getCell(19)));
			source.setUniOrComposition(getStringField(row.getCell(20)));
			source.setOriginalCustomerGSTIN(getStringField(row.getCell(21)));
			source.setCustomerName(getStringField(row.getCell(22)));
			source.setCustomerCode(getStringField(row.getCell(23)));
			source.setBillToState(getStringField(row.getCell(24)));
			source.setShipToState(getStringField(row.getCell(25)));
			source.setPos(getStringField(row.getCell(26)));
			source.setPortCode(getStringField(row.getCell(27)));
			source.setShippingBillNumber(getStringField(row.getCell(28)));
			source.setShippingBillDate(getDateField(row.getCell(29)));
			source.setFob(getStringField(row.getCell(30)));
			source.setExportDuty(getStringField(row.getCell(31)));
			source.setHsnOrSAC(getStringField(row.getCell(32)));
			source.setProductCode(getStringField(row.getCell(33)));
			source.setProductDescription(getStringField(row.getCell(34)));
			source.setCategoryOfProduct(getStringField(row.getCell(35)));
			source.setUnitOfMeasurement(getStringField(row.getCell(36)));
			source.setQuantity((int) (getNumericField(row.getCell(37))));
			source.setTaxableValue(getNumericField(row.getCell(38)));
			source.setIntegratedTaxRate(getNumericField(row.getCell(39)));
			source.setIntegratedTaxAmount(getNumericField(row.getCell(40)));
			source.setCentralTaxRate(getNumericField(row.getCell(41)));
			source.setCentralTaxAmount(getNumericField(row.getCell(42)));
			source.setStateUTTaxRate(getNumericField(row.getCell(43)));
			source.setStateUTTaxAmount(getNumericField(row.getCell(44)));
			source.setCessRateAdvalorem(getNumericField(row.getCell(45)));
			source.setCessAmountAdvalorem(getNumericField(row.getCell(46)));
			source.setCessRateSpecific(getNumericField(row.getCell(47)));
			source.setCessAmountSpecific(getNumericField(row.getCell(48)));
			source.setInvoiceValue(getNumericField(row.getCell(49)));
			source.setReverseChargeFlag(getStringField(row.getCell(50)));
			source.setTcsFlag(getStringField(row.getCell(51)));
			source.seteCommGSTIN(getStringField(row.getCell(52)));
			source.setItcFlag(getStringField(row.getCell(53)));
			source.setReasonForCreditDebitNote(getStringField(row.getCell(54)));
			source.setAccountVoucherNumber(getStringField(row.getCell(55)));
			source.setAccountingVoucherDate(getDateField(row.getCell(56)));
			source.setUserDefinedField1(getStringField(row.getCell(57)));
			source.setUserDefinedField2(getStringField(row.getCell(58)));
			source.setUserDefinedField3(getStringField(row.getCell(59)));

			sourceList.add(source);
		}
		System.out.println("sourceList.size() : " + sourceList.size());
		System.out.println("returnPeriod : " + sourceList.get(0).getReturnPeriod());
		exchange.getIn().setBody(sourceList);
		workbook.close();
	}

	public String getStringField(Cell cell) {
		if (cell != null) {
			DataFormatter df = new DataFormatter();
			return df.formatCellValue(cell);
		}
		return "";
	}

	public Date getDateField(Cell cell) {
		if (cell != null) {
			if(HSSFDateUtil.isCellDateFormatted(cell))
			   {
				return cell.getDateCellValue();
			   }
	        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			try {
				return formatter.parse(cell.getStringCellValue());
			} catch (ParseException e) {
				System.out.println("ParseException : "+e.toString());
				return null;
			}
		}
		Date date = null;
		return date;
	}

	public double getNumericField(Cell cell) {
		if (cell != null) {
			return cell.getNumericCellValue();
		}
		return 0;
	}
}
