package in.co.dhdigital.processors;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import in.co.dhdigital.models.Destination;

public class WriteExcelProcessor implements Processor {
	
	private static String[] columns = { "Supplier GSTIN", "Transaction Type", "Original Transaction Type",
			"Nature of Document", "Customer GSTIN", "Original Customer GSTIN", "E-commerce GSTIN", "Customer Name",
			"ERP Transaction ID", "document no", "document date", "Document Cancellation Date", "Original Document No",
			"Original Document Date", "Line Item ID", "Goods / Services", "HSN", "HSN Description", "quantity",
			"Price per unit", "UQC", "Place of Supply", "Original Place of Supply", "Section 7 Supply",
			"Gross Taxable Value", "Charges Before GST", "Discount Before GST", "Net Taxable Value", "IGST Rate",
			"IGST Amount", "CGST Rate", "CGST Amount", "SGST Rate", "SGST Amount", "Cess Rate", "Cess Amount",
			"Charges After GST", "Discount After GST", "Differential Percentage", "Gross Value (line level)",
			"Invoice Value", "Supply Category", "RCM", "Zero Rated Supply Type", "Shipping Bill No",
			"Shipping Bill Date", "port", "Division", "Sub-location", "Ship to State Code (Customer)",
			"Ship from State", "Financial period (mmyyyy)", "Customer GL Id Number", "Customer GL Id Name",
			"Income GL Id Number", "Income GL id Name", "IGST GL id No", "CGST GL id No", "SGST GL Id No",
			"Cess GL Id No", "Customer ID", "Additional Column 1", "Additional Column 2", "Additional Column 3",
			"Additional Column 4", "Additional Column 5", "Additional Column 6", "Additional Column 7",
			"Additional Column 8", "Additional Column 9", "Additional Column 10", "Additional Column 11",
			"Additional Column 12", "Additional Column 13", "Additional Column 14", "Additional Column 15", "Remarks" };

	@Override
	public void process(Exchange exchange) throws Exception {
		List<Destination> destinationList = new ArrayList<Destination>();
		destinationList = (List<Destination>) exchange.getIn().getBody();

		System.out.println("Inside ErrorProcessor.process()");
		Workbook workbook = new XSSFWorkbook();
		CreationHelper createHelper = workbook.getCreationHelper();

		// Create Sheet.
		Sheet sheet = workbook.createSheet("Destination");

		// Create a row.
		Row headerRow = sheet.createRow(0);
		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
		int rowNum = 1;
		
		for(Destination destination: destinationList) {
			if(destination.getRemarks() == null || (destination.getRemarks() != null && destination.getRemarks().equals(""))) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(destination.getSupplierGSTIN());
			row.createCell(1).setCellValue(destination.getTransactionType());
			row.createCell(2).setCellValue(destination.getOriginalTransactionType());
			row.createCell(3).setCellValue(destination.getNatureOfDocument());
			row.createCell(4).setCellValue(destination.getCustomerGSTIN());
			row.createCell(5).setCellValue(destination.getOriginalCustomerGSTIN());
			row.createCell(6).setCellValue(destination.geteCommGSTIN());
			row.createCell(7).setCellValue(destination.getCustomerName());
			row.createCell(8).setCellValue(destination.getErpTransactionID());
			row.createCell(9).setCellValue(destination.getDocumentNumber());
			row.createCell(10).setCellValue(destination.getDocumentDate());
			row.createCell(11).setCellValue(destination.getDocumentCancellationDate());
			row.createCell(12).setCellValue(destination.getOriginalDocumentNumber());
			row.createCell(13).setCellValue(destination.getOriginalDocumentDate());
			row.createCell(14).setCellValue(destination.getLineItemID());
			row.createCell(15).setCellValue(destination.getGoodsOrService());
			row.createCell(16).setCellValue(destination.getHsn());
			row.createCell(17).setCellValue(destination.getHsnDescription());
			row.createCell(18).setCellValue(destination.getQuantity());
			row.createCell(19).setCellValue(destination.getPricePerUnit());
			row.createCell(20).setCellValue(destination.getUqc());
			row.createCell(21).setCellValue(destination.getPlaceOfSupply());
			row.createCell(22).setCellValue(destination.getOriginalPlaceOfSupply());
			row.createCell(23).setCellValue(destination.getSection7Supply());
			row.createCell(24).setCellValue(destination.getGrossTaxableValue());
			row.createCell(25).setCellValue(destination.getChargesBeforeGST());
			row.createCell(26).setCellValue(destination.getDiscountBeforeGST());
			row.createCell(27).setCellValue(destination.getNetTaxableValue());
			row.createCell(28).setCellValue(destination.getIgstRate());
			row.createCell(29).setCellValue(destination.getIgstAmount());
			row.createCell(30).setCellValue(destination.getCgstRate());
			row.createCell(31).setCellValue(destination.getCgstAmount());
			row.createCell(32).setCellValue(destination.getSgstRate());
			row.createCell(33).setCellValue(destination.getSgstAmount());
			row.createCell(34).setCellValue(destination.getCessRate());
			row.createCell(35).setCellValue(destination.getCessAmount());
			row.createCell(36).setCellValue(destination.getChargesAfterGST());
			row.createCell(37).setCellValue(destination.getDiscountAfterGST());
			row.createCell(38).setCellValue(destination.getDifferentialPercentage());
			row.createCell(39).setCellValue(destination.getGrossValueLineLevel());
			row.createCell(40).setCellValue(destination.getInvoiceValue());
			row.createCell(41).setCellValue(destination.getSupplyCategory());
			row.createCell(42).setCellValue(destination.getRcm());
			row.createCell(43).setCellValue(destination.getZeroRatedSupplyType());
			row.createCell(44).setCellValue(destination.getShippingBillNumber());
			row.createCell(45).setCellValue(destination.getShippingBillDate());
			row.createCell(46).setCellValue(destination.getPort());
			row.createCell(47).setCellValue(destination.getDivision());
			row.createCell(48).setCellValue(destination.getSubLocation());
			row.createCell(49).setCellValue(destination.getShipToStateCode());
			row.createCell(50).setCellValue(destination.getShipFromState());
			row.createCell(51).setCellValue(destination.getFinancialPeriod());
			row.createCell(52).setCellValue(destination.getCustomerGLIdNumber());
			row.createCell(53).setCellValue(destination.getCustomerGLIdName());
			row.createCell(54).setCellValue(destination.getIncomeGLIdNumber());
			row.createCell(55).setCellValue(destination.getIncomeGLIdName());
			row.createCell(56).setCellValue(destination.getIgstGLIdNo());
			row.createCell(57).setCellValue(destination.getCgstGLIdNo());
			row.createCell(58).setCellValue(destination.getSgstGLIdNo());
			row.createCell(59).setCellValue(destination.getCessGLIdNo());
			row.createCell(60).setCellValue(destination.getCustomerID());
			row.createCell(61).setCellValue(destination.getAdditionalColumn1());
			row.createCell(62).setCellValue(destination.getAdditionalColumn2());
			row.createCell(63).setCellValue(destination.getAdditionalColumn3());
			row.createCell(64).setCellValue(destination.getAdditionalColumn4());
			row.createCell(65).setCellValue(destination.getAdditionalColumn5());
			row.createCell(66).setCellValue(destination.getAdditionalColumn6());
			row.createCell(67).setCellValue(destination.getAdditionalColumn7());
			row.createCell(68).setCellValue(destination.getAdditionalColumn8());
			row.createCell(69).setCellValue(destination.getAdditionalColumn9());
			row.createCell(70).setCellValue(destination.getAdditionalColumn10());
			row.createCell(71).setCellValue(destination.getAdditionalColumn11());
			row.createCell(72).setCellValue(destination.getAdditionalColumn12());
			row.createCell(73).setCellValue(destination.getAdditionalColumn13());
			row.createCell(74).setCellValue(destination.getAdditionalColumn14());
			row.createCell(75).setCellValue(destination.getAdditionalColumn15());
			
			}

		}
		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
					sheet.autoSizeColumn(i);
		}// Write the output to a file
		// FileOutputStream fileOut = new FileOutputStream("poi-generated-file.xlsx");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		workbook.write(bos);
		bos.close();

		// fileOut.close();
		byte[] xlsx = bos.toByteArray();

		exchange.getIn().setBody(xlsx);

		// Closing the workbook
		workbook.close();

			
	}

}
