package in.co.dhdigital.repositories;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import in.co.dhdigital.models.Destination;

@Repository
public interface DestinationRepository extends CassandraRepository<Destination>{

}
