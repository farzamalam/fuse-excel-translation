package in.co.dhdigital.models;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "destination")
public class Destination {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	
	@Column(name= "supplier_gstin")
	private String supplierGSTIN;

	@Column(name= "transaction_type")
	private String transactionType;

	@Column(name= "original_transaction_type")
	private String originalTransactionType;

	@Column(name= "nature_of_document")
	private String natureOfDocument;

	@Column(name= "customer_gstin")
	private String customerGSTIN;

	@Column(name= "original_customer_gstin")
	private String originalCustomerGSTIN;

	@Column(name= "ecomm_gstin")
	private String eCommGSTIN;

	@Column(name= "customer_name")
	private String customerName;

	@Column(name= "erp_transaction_id")
	private String erpTransactionID;

	@Column(name= "document_number")
	private String documentNumber;


	@Column(name= "document_date")
	private Date documentDate;

	@Column(name= "document_cancellation_date")
	private Date documentCancellationDate;

	@Column(name= "original_document_number")
	private String originalDocumentNumber;

	@Column(name= "original_document_date")
	private Date originalDocumentDate;

	@Column(name= "line_item_id")
	private int lineItemID;

	@Column(name= "goods_or_service")
	private String goodsOrService;

	@Column(name= "hsn")
	private String hsn;

	@Column(name= "hsn_description")
	private String hsnDescription;

	@Column(name= "quantity")
	private int quantity;

	@Column(name= "price_per_unit")
	private double pricePerUnit;

	@Column(name= "uqc")
	private String uqc;

	@Column(name= "place_of_supply")
	private String placeOfSupply;

	@Column(name= "original_place_of_supply")
	private String originalPlaceOfSupply;

	@Column(name= "section7_supply")
	private String section7Supply;

	@Column(name= "gross_taxable_value")
	private double grossTaxableValue;


	@Column(name= "charges_before_gst")
	private double chargesBeforeGST;

	@Column(name= "discount_before_gst")
	private double discountBeforeGST;

	@Column(name= "net_taxable_value")
	private double netTaxableValue;


	@Column(name= "igst_rate")
	private double igstRate;

	@Column(name= "igst_amount")
	private double igstAmount;

	@Column(name= "cgst_rate")
	private double cgstRate;

	@Column(name= "cgst_amount")
	private double cgstAmount;

	@Column(name= "sgst_rate")
	private double sgstRate;

	@Column(name= "sgst_amount")
	private double sgstAmount;

	@Column(name= "cess_rate")
	private double cessRate;

	@Column(name= "cess_amount")
	private double cessAmount;


	@Column(name= "charges_after_gst")
	private double chargesAfterGST;

	@Column(name= "discount_after_gst")
	private double discountAfterGST;

	@Column(name= "differential_percentage")
	private double differentialPercentage;

	@Column(name= "gross_value_line_level")
	private double grossValueLineLevel;

	@Column(name= "invoice_value")
	private double invoiceValue;

	@Column(name= "supply_category")
	private String supplyCategory;

	@Column(name= "rcm")
	private String rcm;

	@Column(name= "zero_rated_supply_type")
	private String zeroRatedSupplyType;

	@Column(name= "shipping_bill_number")
	private String shippingBillNumber;

	@Column(name= "shipping_bill_date")
	private Date shippingBillDate;

	@Column(name= "port")
	private String port;

	@Column(name= "division")
	private String division;

	@Column(name= "sub_division")
	private String subDivision;

	@Column(name= "sub_location")
	private String subLocation;

	@Column(name= "ship_to_state_code")
	private String shipToStateCode;

	@Column(name= "ship_from_state")
	private String shipFromState;

	@Column(name= "financial_period")
	private String financialPeriod;

	@Column(name= "customer_glid_number")
	private String customerGLIdNumber;

	@Column(name= "customer_glid_name")
	private String customerGLIdName;

	@Column(name= "income_gli_number")
	private String incomeGLIdNumber;


	@Column(name= "income_glid_name")
	private String incomeGLIdName;

	@Column(name= "igst_glid_no")
	private String igstGLIdNo;

	@Column(name= "cgst_glid_no")
	private String cgstGLIdNo;

	@Column(name= "sgst_glid_no")
	private String sgstGLIdNo;

	@Column(name = "cess_glid_no")
	private String cessGLIdNo;
	
	@Column(name= "customer_id")
	private String customerID;

	@Column(name= "additional_column1")
	private String additionalColumn1;

	@Column(name= "additional_column2")
	private String additionalColumn2;

	@Column(name= "additional_column3")
	private String additionalColumn3;

	@Column(name= "additional_column4")
	private String additionalColumn4;

	@Column(name= "additional_column5")
	private String additionalColumn5;

	@Column(name= "additional_column6")
	private String additionalColumn6;

	@Column(name= "additional_colum7")
	private String additionalColumn7;

	@Column(name= "additional_column8")
	private String additionalColumn8;

	@Column(name= "additional_column9")
	private String additionalColumn9;

	@Column(name= "additional_column10")
	private String additionalColumn10;

	@Column(name= "additional_column11")
	private String additionalColumn11;

	@Column(name= "additional_column12")
	private String additionalColumn12;

	@Column(name= "additional_column13")
	private String additionalColumn13;

	@Column(name= "additional_column14")
	private String additionalColumn14;

	@Column(name= "additional_column15")
	private String additionalColumn15;

	@Column(name = "remarks")
	private String remarks;


	
	// Getters and Setters.
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getSupplierGSTIN() {
		return supplierGSTIN;
	}

	public void setSupplierGSTIN(String supplierGSTIN) {
		this.supplierGSTIN = supplierGSTIN;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getOriginalTransactionType() {
		return originalTransactionType;
	}

	public void setOriginalTransactionType(String originalTransactionType) {
		this.originalTransactionType = originalTransactionType;
	}

	public String getNatureOfDocument() {
		return natureOfDocument;
	}

	public String getCessGLIdNo() {
		return cessGLIdNo;
	}

	public void setCessGLIdNo(String cessGLIdNo) {
		this.cessGLIdNo = cessGLIdNo;
	}

	public void setNatureOfDocument(String natureOfDocument) {
		this.natureOfDocument = natureOfDocument;
	}

	public String getCustomerGSTIN() {
		return customerGSTIN;
	}

	public void setCustomerGSTIN(String customerGSTIN) {
		this.customerGSTIN = customerGSTIN;
	}

	public String getOriginalCustomerGSTIN() {
		return originalCustomerGSTIN;
	}

	public void setOriginalCustomerGSTIN(String originalCustomerGSTIN) {
		this.originalCustomerGSTIN = originalCustomerGSTIN;
	}

	public String geteCommGSTIN() {
		return eCommGSTIN;
	}

	public void seteCommGSTIN(String eCommGSTIN) {
		this.eCommGSTIN = eCommGSTIN;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getErpTransactionID() {
		return erpTransactionID;
	}

	public void setErpTransactionID(String erpTransactionID) {
		this.erpTransactionID = erpTransactionID;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public Date getDocumentCancellationDate() {
		return documentCancellationDate;
	}

	public void setDocumentCancellationDate(Date documentCancellationDate) {
		this.documentCancellationDate = documentCancellationDate;
	}

	public String getOriginalDocumentNumber() {
		return originalDocumentNumber;
	}

	public void setOriginalDocumentNumber(String originalDocumentNumber) {
		this.originalDocumentNumber = originalDocumentNumber;
	}

	public Date getOriginalDocumentDate() {
		return originalDocumentDate;
	}

	public void setOriginalDocumentDate(Date originalDocumentDate) {
		this.originalDocumentDate = originalDocumentDate;
	}

	public int getLineItemID() {
		return lineItemID;
	}

	public void setLineItemID(int lineItemID) {
		this.lineItemID = lineItemID;
	}

	public String getGoodsOrService() {
		return goodsOrService;
	}

	public void setGoodsOrService(String goodsOrService) {
		this.goodsOrService = goodsOrService;
	}

	public String getHsn() {
		return hsn;
	}

	public void setHsn(String hsn) {
		this.hsn = hsn;
	}

	public String getHsnDescription() {
		return hsnDescription;
	}

	public void setHsnDescription(String hsnDescription) {
		this.hsnDescription = hsnDescription;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public String getUqc() {
		return uqc;
	}

	public void setUqc(String uqc) {
		this.uqc = uqc;
	}

	public String getPlaceOfSupply() {
		return placeOfSupply;
	}

	public void setPlaceOfSupply(String placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	}

	public String getOriginalPlaceOfSupply() {
		return originalPlaceOfSupply;
	}

	public void setOriginalPlaceOfSupply(String originalPlaceOfSupply) {
		this.originalPlaceOfSupply = originalPlaceOfSupply;
	}

	public String getSection7Supply() {
		return section7Supply;
	}

	public void setSection7Supply(String section7Supply) {
		this.section7Supply = section7Supply;
	}

	public double getGrossTaxableValue() {
		return grossTaxableValue;
	}

	public void setGrossTaxableValue(double grossTaxableValue) {
		this.grossTaxableValue = grossTaxableValue;
	}

	public double getChargesBeforeGST() {
		return chargesBeforeGST;
	}

	public void setChargesBeforeGST(double chargesBeforeGST) {
		this.chargesBeforeGST = chargesBeforeGST;
	}

	public double getDiscountBeforeGST() {
		return discountBeforeGST;
	}

	public void setDiscountBeforeGST(double discountBeforeGST) {
		this.discountBeforeGST = discountBeforeGST;
	}

	public double getNetTaxableValue() {
		return netTaxableValue;
	}

	public void setNetTaxableValue(double netTaxableValue) {
		this.netTaxableValue = netTaxableValue;
	}

	public double getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(double igstRate) {
		this.igstRate = igstRate;
	}

	public double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public double getCgstRate() {
		return cgstRate;
	}

	public void setCgstRate(double cgstRate) {
		this.cgstRate = cgstRate;
	}

	public double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public double getSgstRate() {
		return sgstRate;
	}

	public void setSgstRate(double sgstRate) {
		this.sgstRate = sgstRate;
	}

	public double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public double getCessRate() {
		return cessRate;
	}

	public void setCessRate(double cessRate) {
		this.cessRate = cessRate;
	}

	public double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public double getChargesAfterGST() {
		return chargesAfterGST;
	}

	public void setChargesAfterGST(double chargesAfterGST) {
		this.chargesAfterGST = chargesAfterGST;
	}

	public double getDiscountAfterGST() {
		return discountAfterGST;
	}

	public void setDiscountAfterGST(double discountAfterGST) {
		this.discountAfterGST = discountAfterGST;
	}

	public double getDifferentialPercentage() {
		return differentialPercentage;
	}

	public void setDifferentialPercentage(double differentialPercentage) {
		this.differentialPercentage = differentialPercentage;
	}

	public double getGrossValueLineLevel() {
		return grossValueLineLevel;
	}

	public void setGrossValueLineLevel(double grossValueLineLevel) {
		this.grossValueLineLevel = grossValueLineLevel;
	}

	public double getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public Date getShippingBillDate() {
		return shippingBillDate;
	}

	public void setShippingBillDate(Date shippingBillDate) {
		this.shippingBillDate = shippingBillDate;
	}
	
	

	public String getSupplyCategory() {
		return supplyCategory;
	}

	public void setSupplyCategory(String supplyCategory) {
		this.supplyCategory = supplyCategory;
	}

	public String getRcm() {
		return rcm;
	}

	public void setRcm(String rcm) {
		this.rcm = rcm;
	}

	public String getZeroRatedSupplyType() {
		return zeroRatedSupplyType;
	}

	public void setZeroRatedSupplyType(String zeroRatedSupplyType) {
		this.zeroRatedSupplyType = zeroRatedSupplyType;
	}

	public String getShippingBillNumber() {
		return shippingBillNumber;
	}

	public void setShippingBillNumber(String shippingBillNumber) {
		this.shippingBillNumber = shippingBillNumber;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSubDivision() {
		return subDivision;
	}

	public void setSubDivision(String subDivision) {
		this.subDivision = subDivision;
	}

	public String getSubLocation() {
		return subLocation;
	}

	public void setSubLocation(String subLocation) {
		this.subLocation = subLocation;
	}

	public String getShipToStateCode() {
		return shipToStateCode;
	}

	public void setShipToStateCode(String shipToStateCode) {
		this.shipToStateCode = shipToStateCode;
	}

	public String getShipFromState() {
		return shipFromState;
	}

	public void setShipFromState(String shipFromState) {
		this.shipFromState = shipFromState;
	}

	public String getFinancialPeriod() {
		return financialPeriod;
	}

	public void setFinancialPeriod(String financialPeriod) {
		this.financialPeriod = financialPeriod;
	}

	public String getCustomerGLIdNumber() {
		return customerGLIdNumber;
	}

	public void setCustomerGLIdNumber(String customerGLIdNumber) {
		this.customerGLIdNumber = customerGLIdNumber;
	}

	public String getCustomerGLIdName() {
		return customerGLIdName;
	}

	public void setCustomerGLIdName(String customerGLIdName) {
		this.customerGLIdName = customerGLIdName;
	}

	public String getIncomeGLIdNumber() {
		return incomeGLIdNumber;
	}

	public void setIncomeGLIdNumber(String incomeGLIdNumber) {
		this.incomeGLIdNumber = incomeGLIdNumber;
	}

	public String getIncomeGLIdName() {
		return incomeGLIdName;
	}

	public void setIncomeGLIdName(String incomeGLIdName) {
		this.incomeGLIdName = incomeGLIdName;
	}

	public String getIgstGLIdNo() {
		return igstGLIdNo;
	}

	public void setIgstGLIdNo(String igstGLIdNo) {
		this.igstGLIdNo = igstGLIdNo;
	}

	public String getCgstGLIdNo() {
		return cgstGLIdNo;
	}

	public void setCgstGLIdNo(String cgstGLIdNo) {
		this.cgstGLIdNo = cgstGLIdNo;
	}

	public String getSgstGLIdNo() {
		return sgstGLIdNo;
	}

	public void setSgstGLIdNo(String sgstGLIdNo) {
		this.sgstGLIdNo = sgstGLIdNo;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public String getAdditionalColumn1() {
		return additionalColumn1;
	}

	public void setAdditionalColumn1(String additionalColumn1) {
		this.additionalColumn1 = additionalColumn1;
	}

	public String getAdditionalColumn2() {
		return additionalColumn2;
	}

	public void setAdditionalColumn2(String additionalColumn2) {
		this.additionalColumn2 = additionalColumn2;
	}

	public String getAdditionalColumn3() {
		return additionalColumn3;
	}

	public void setAdditionalColumn3(String additionalColumn3) {
		this.additionalColumn3 = additionalColumn3;
	}

	public String getAdditionalColumn4() {
		return additionalColumn4;
	}

	public void setAdditionalColumn4(String additionalColumn4) {
		this.additionalColumn4 = additionalColumn4;
	}

	public String getAdditionalColumn5() {
		return additionalColumn5;
	}

	public void setAdditionalColumn5(String additionalColumn5) {
		this.additionalColumn5 = additionalColumn5;
	}

	public String getAdditionalColumn6() {
		return additionalColumn6;
	}

	public void setAdditionalColumn6(String additionalColumn6) {
		this.additionalColumn6 = additionalColumn6;
	}

	public String getAdditionalColumn7() {
		return additionalColumn7;
	}

	public void setAdditionalColumn7(String additionalColumn7) {
		this.additionalColumn7 = additionalColumn7;
	}

	public String getAdditionalColumn8() {
		return additionalColumn8;
	}

	public void setAdditionalColumn8(String additionalColumn8) {
		this.additionalColumn8 = additionalColumn8;
	}

	public String getAdditionalColumn9() {
		return additionalColumn9;
	}

	public void setAdditionalColumn9(String additionalColumn9) {
		this.additionalColumn9 = additionalColumn9;
	}

	public String getAdditionalColumn10() {
		return additionalColumn10;
	}

	public void setAdditionalColumn10(String additionalColumn10) {
		this.additionalColumn10 = additionalColumn10;
	}

	public String getAdditionalColumn11() {
		return additionalColumn11;
	}

	public void setAdditionalColumn11(String additionalColumn11) {
		this.additionalColumn11 = additionalColumn11;
	}

	public String getAdditionalColumn12() {
		return additionalColumn12;
	}

	public void setAdditionalColumn12(String additionalColumn12) {
		this.additionalColumn12 = additionalColumn12;
	}

	public String getAdditionalColumn13() {
		return additionalColumn13;
	}

	public void setAdditionalColumn13(String additionalColumn13) {
		this.additionalColumn13 = additionalColumn13;
	}

	public String getAdditionalColumn14() {
		return additionalColumn14;
	}

	public void setAdditionalColumn14(String additionalColumn14) {
		this.additionalColumn14 = additionalColumn14;
	}

	public String getAdditionalColumn15() {
		return additionalColumn15;
	}

	public void setAdditionalColumn15(String additionalColumn15) {
		this.additionalColumn15 = additionalColumn15;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	

}
























