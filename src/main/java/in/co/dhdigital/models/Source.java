package in.co.dhdigital.models;

import java.util.Date;

public class Source {
	
	private String sourceIdenitifier;
	private String sourceFileName;
	private String glaAccountCode;
	private String division;
	private String subDivision;
	private String profitCenter1;
	private String profitCenter2;
	private String plantCode;
	private String returnPeriod;
	private String supplierGSTIN;
	private String state;
	private String documentType;
	private String supplyType;
	private String documentNumber;
	private Date documentDate;
	private String originalDocumentNumber;
	private Date originalDocumentDate;
	private String crdrPreGST;
	private int lineNumber;
	private String customerGSTIN;
	private String uniOrComposition;
	private String originalCustomerGSTIN;
	private String customerName;
	private String customerCode;
	private String billToState;
	private String shipToState;
	private String pos;
	private String portCode;
	private String shippingBillNumber;
	private Date shippingBillDate;
	private String fob;
	private String exportDuty;
	private String hsnOrSAC;
	private String productCode;
	private String productDescription;
	private String categoryOfProduct;
	private String unitOfMeasurement;
	private int quantity;
	private double taxableValue;
	private double integratedTaxRate;
	private double integratedTaxAmount;
	private double centralTaxRate;
	private double centralTaxAmount;
	private double stateUTTaxRate;
	private double stateUTTaxAmount;
	private double cessRateAdvalorem;
	private double cessAmountAdvalorem;
	private double cessRateSpecific;
	private double cessAmountSpecific;
	private double invoiceValue;
	private String reverseChargeFlag;
	private String tcsFlag;
	private String eCommGSTIN;
	private String itcFlag;
	private String reasonForCreditDebitNote;
	private String accountVoucherNumber;
	private Date accountingVoucherDate;
	private String userDefinedField1;
	private String userDefinedField2;
	private String userDefinedField3;
	
	
	// Getters and Setters.
	public String getSourceIdenitifier() {
		return sourceIdenitifier;
	}
	public void setSourceIdenitifier(String sourceIdenitifier) {
		this.sourceIdenitifier = sourceIdenitifier;
	}
	public String getSourceFileName() {
		return sourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	public String getGlaAccountCode() {
		return glaAccountCode;
	}
	public void setGlaAccountCode(String glaAccountCode) {
		this.glaAccountCode = glaAccountCode;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getSubDivision() {
		return subDivision;
	}
	public void setSubDivision(String subDivision) {
		this.subDivision = subDivision;
	}
	public String getProfitCenter1() {
		return profitCenter1;
	}
	public void setProfitCenter1(String profitCenter1) {
		this.profitCenter1 = profitCenter1;
	}
	public String getProfitCenter2() {
		return profitCenter2;
	}
	public void setProfitCenter2(String profitCenter2) {
		this.profitCenter2 = profitCenter2;
	}
	public String getPlantCode() {
		return plantCode;
	}
	public void setPlantCode(String plantCode) {
		this.plantCode = plantCode;
	}
	public String getReturnPeriod() {
		return returnPeriod;
	}
	public void setReturnPeriod(String returnPeriod) {
		this.returnPeriod = returnPeriod;
	}
	public String getSupplierGSTIN() {
		return supplierGSTIN;
	}
	public void setSupplierGSTIN(String supplierGSTIN) {
		this.supplierGSTIN = supplierGSTIN;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getSupplyType() {
		return supplyType;
	}
	public void setSupplyType(String supplyType) {
		this.supplyType = supplyType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Date getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}
	public String getOriginalDocumentNumber() {
		return originalDocumentNumber;
	}
	public void setOriginalDocumentNumber(String originalDocumentNumber) {
		this.originalDocumentNumber = originalDocumentNumber;
	}
	public Date getOriginalDocumentDate() {
		return originalDocumentDate;
	}
	public void setOriginalDocumentDate(Date originalDocumentDate) {
		this.originalDocumentDate = originalDocumentDate;
	}
	public String getCrdrPreGST() {
		return crdrPreGST;
	}
	public void setCrdrPreGST(String crdrPreGST) {
		this.crdrPreGST = crdrPreGST;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getCustomerGSTIN() {
		return customerGSTIN;
	}
	public void setCustomerGSTIN(String customerGSTIN) {
		this.customerGSTIN = customerGSTIN;
	}
	public String getUniOrComposition() {
		return uniOrComposition;
	}
	public void setUniOrComposition(String uniOrComposition) {
		this.uniOrComposition = uniOrComposition;
	}
	public String getOriginalCustomerGSTIN() {
		return originalCustomerGSTIN;
	}
	public void setOriginalCustomerGSTIN(String originalCustomerGSTIN) {
		this.originalCustomerGSTIN = originalCustomerGSTIN;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getBillToState() {
		return billToState;
	}
	public void setBillToState(String billToState) {
		this.billToState = billToState;
	}
	public String getShipToState() {
		return shipToState;
	}
	public void setShipToState(String shipToState) {
		this.shipToState = shipToState;
	}
	public String getPos() {
		return pos;
	}
	public void setPos(String pos) {
		this.pos = pos;
	}
	public String getPortCode() {
		return portCode;
	}
	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}
	public String getShippingBillNumber() {
		return shippingBillNumber;
	}
	public void setShippingBillNumber(String shippingBillNumber) {
		this.shippingBillNumber = shippingBillNumber;
	}
	public Date getShippingBillDate() {
		return shippingBillDate;
	}
	public void setShippingBillDate(Date shippingBillDate) {
		this.shippingBillDate = shippingBillDate;
	}
	public String getFob() {
		return fob;
	}
	public void setFob(String fob) {
		this.fob = fob;
	}
	public String getExportDuty() {
		return exportDuty;
	}
	public void setExportDuty(String exportDuty) {
		this.exportDuty = exportDuty;
	}
	public String getHsnOrSAC() {
		return hsnOrSAC;
	}
	public void setHsnOrSAC(String hsnOrSAC) {
		this.hsnOrSAC = hsnOrSAC;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getCategoryOfProduct() {
		return categoryOfProduct;
	}
	public void setCategoryOfProduct(String categoryOfProduct) {
		this.categoryOfProduct = categoryOfProduct;
	}
	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getTaxableValue() {
		return taxableValue;
	}
	public void setTaxableValue(double taxableValue) {
		this.taxableValue = taxableValue;
	}
	public double getIntegratedTaxRate() {
		return integratedTaxRate;
	}
	public void setIntegratedTaxRate(double integratedTaxRate) {
		this.integratedTaxRate = integratedTaxRate;
	}
	public double getIntegratedTaxAmount() {
		return integratedTaxAmount;
	}
	public void setIntegratedTaxAmount(double integratedTaxAmount) {
		this.integratedTaxAmount = integratedTaxAmount;
	}
	public double getCentralTaxRate() {
		return centralTaxRate;
	}
	public void setCentralTaxRate(double centralTaxRate) {
		this.centralTaxRate = centralTaxRate;
	}
	public double getCentralTaxAmount() {
		return centralTaxAmount;
	}
	public void setCentralTaxAmount(double centralTaxAmount) {
		this.centralTaxAmount = centralTaxAmount;
	}
	public double getStateUTTaxRate() {
		return stateUTTaxRate;
	}
	public void setStateUTTaxRate(double stateUTTaxRate) {
		this.stateUTTaxRate = stateUTTaxRate;
	}
	public double getStateUTTaxAmount() {
		return stateUTTaxAmount;
	}
	public void setStateUTTaxAmount(double stateUTTaxAmount) {
		this.stateUTTaxAmount = stateUTTaxAmount;
	}
	public double getCessRateAdvalorem() {
		return cessRateAdvalorem;
	}
	public void setCessRateAdvalorem(double cessRateAdvalorem) {
		this.cessRateAdvalorem = cessRateAdvalorem;
	}
	public double getCessAmountAdvalorem() {
		return cessAmountAdvalorem;
	}
	public void setCessAmountAdvalorem(double cessAmountAdvalorem) {
		this.cessAmountAdvalorem = cessAmountAdvalorem;
	}
	public double getCessRateSpecific() {
		return cessRateSpecific;
	}
	public void setCessRateSpecific(double cessRateSpecific) {
		this.cessRateSpecific = cessRateSpecific;
	}
	public double getCessAmountSpecific() {
		return cessAmountSpecific;
	}
	public void setCessAmountSpecific(double cessAmountSpecific) {
		this.cessAmountSpecific = cessAmountSpecific;
	}
	public double getInvoiceValue() {
		return invoiceValue;
	}
	public void setInvoiceValue(double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}
	public String getReverseChargeFlag() {
		return reverseChargeFlag;
	}
	public void setReverseChargeFlag(String reverseChargeFlag) {
		this.reverseChargeFlag = reverseChargeFlag;
	}
	public String getTcsFlag() {
		return tcsFlag;
	}
	public void setTcsFlag(String tcsFlag) {
		this.tcsFlag = tcsFlag;
	}
	public String geteCommGSTIN() {
		return eCommGSTIN;
	}
	public void seteCommGSTIN(String eCommGSTIN) {
		this.eCommGSTIN = eCommGSTIN;
	}
	public String getItcFlag() {
		return itcFlag;
	}
	public void setItcFlag(String itcFlag) {
		this.itcFlag = itcFlag;
	}
	public String getReasonForCreditDebitNote() {
		return reasonForCreditDebitNote;
	}
	public void setReasonForCreditDebitNote(String reasonForCreditDebitNote) {
		this.reasonForCreditDebitNote = reasonForCreditDebitNote;
	}
	public String getAccountVoucherNumber() {
		return accountVoucherNumber;
	}
	public void setAccountVoucherNumber(String accountVoucherNumber) {
		this.accountVoucherNumber = accountVoucherNumber;
	}
	public Date getAccountingVoucherDate() {
		return accountingVoucherDate;
	}
	public void setAccountingVoucherDate(Date accountingVoucherDate) {
		this.accountingVoucherDate = accountingVoucherDate;
	}
	public String getUserDefinedField1() {
		return userDefinedField1;
	}
	public void setUserDefinedField1(String userDefinedField1) {
		this.userDefinedField1 = userDefinedField1;
	}
	public String getUserDefinedField2() {
		return userDefinedField2;
	}
	public void setUserDefinedField2(String userDefinedField2) {
		this.userDefinedField2 = userDefinedField2;
	}
	public String getUserDefinedField3() {
		return userDefinedField3;
	}
	public void setUserDefinedField3(String userDefinedField3) {
		this.userDefinedField3 = userDefinedField3;
	}
	
	
	
}









