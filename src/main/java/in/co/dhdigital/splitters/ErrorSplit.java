package in.co.dhdigital.splitters;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import in.co.dhdigital.models.Destination;

public class ErrorSplit implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		List<Destination> destinationList = new ArrayList<Destination>();
		List<Destination> destinationWithoutError= new ArrayList<Destination>();

		destinationList = (List<Destination>) exchange.getIn().getBody();

		System.out.println("Inside ErrorSplit.process()");
		for(Destination destination : destinationList) {
			if(destination.getRemarks() == null) {
				destinationWithoutError.add(destination);
			}
		}
		System.out.println("destinationWithoutError.size() : "+ destinationWithoutError.size());
		exchange.getIn().setBody(destinationWithoutError);
	}

}
